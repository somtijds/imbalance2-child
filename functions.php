<?php

/**
 * @package Imbalance2 Child Theme
 */

/**
 * Load the parent and child theme styles
 */
function ft_parent_theme_style() {

	// Parent theme styles
	wp_enqueue_style( 'ft-style', get_template_directory_uri(). '/style.css' );

	// Child theme styles
	wp_enqueue_style( 'ft-child-style', get_stylesheet_directory_uri(). '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'ft_parent_theme_style',10 );

add_action( 'after_setup_theme', 'ft_woocommerce_support' );

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_before_main_content','woocommerce_breadcrumb', 20);

remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_before_shop_loop','woocommerce_catalog_ordering',30 );
add_action('woocommerce_before_main_content', 'ft_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'ft_wrapper_end', 10);


add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'ft_add_size_to_griditem', 9 );

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

function ft_woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

function ft_wrapper_start() {
  echo '<section id="main">';
}

function ft_wrapper_end() {
  echo '</section>';
}

function ft_add_size_to_griditem() {
	global $product;
	$size = $product->get_attribute('pa_afmetingen');
	if ( ! empty( $size ) ) {
		echo '<span class="ft_grid-product-size">' . $size . '</span>';
	}
}

/* CUSTOMISED WOOCOMMERCE TEMPLATE FUNCTIONS */

/**
 * Show subcategory thumbnails.
 *
 * @param mixed $category Category.
 * @subpackage	Loop
 */
function woocommerce_subcategory_thumbnail( $category ) {
	$small_thumbnail_size  	= apply_filters( 'subcategory_archive_thumbnail_size', 'shop_catalog' );
	$dimensions    			= wc_get_image_size( $small_thumbnail_size );
	$thumbnail_id  			= get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );

	if ( $thumbnail_id ) {
		$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
		$image        = $image[0];
		$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
		$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
	} else {
		$image        = wc_placeholder_img_src();
		$image_srcset = $image_sizes = false;
	}

	if ( $image ) {
		// Prevent esc_url from breaking spaces in urls for image embeds.
		// Ref: https://core.trac.wordpress.org/ticket/23605.
		$image = str_replace( ' ', '%20', $image );
		echo '<div class="ft_grid-image-wrapper">';
		// Add responsive image markup if available.
		if ( $image_srcset && $image_sizes ) {
			echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
		} else {
			echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" />';
		}
		echo '</div>';
	}
}

/**
 * Get the product thumbnail, or the placeholder if not set.
 *
 * @subpackage	Loop
 * @param string $size (default: 'shop_catalog').
 * @param int    $deprecated1 Deprecated since WooCommerce 2.0 (default: 0).
 * @param int    $deprecated2 Deprecated since WooCommerce 2.0 (default: 0).
 * @return string
 */
function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0 ) {
	global $product;

	$image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );

	return $product ? '<div class="ft_grid-image-wrapper">' . $product->get_image( $image_size ) . '</div>' : '';
}

